//
// Consts and functions
const months = [
    `janvier`,
    `février`,
    `mars`,
    `avril`,
    `mai`,
    `juin`,
    `juillet`,
    `août`,
    `septembre`,
    `octobre`,
    `novembre`,
    `décembre`
];

const days = [
    `lundi`,
    `mardi`,
    `mercredi`,
    `jeudi`,
    `vendredi`,
    `samedi`,
    `dimanche`
];

const reg = /^([lL]undi|[mM]ardi|[mM]ercredi|[jJ]eudi|[vV]endredi|[sS]amedi|[dD]imanche) (0[1-9]|[1-2][0-9]|3[01]) ([jJ]anvier|[fF]évrier|[mM]ars|[aA]vril|[mM]ai|[jJ]uin|[jJ]uillet|[aA]oût|[sS]eptembre|[oO]ctobre|[nN]ovembre|[dD]écembre) ([0-9]{4}) ([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/gm;

function die_wrong()
{
    console.log(`Wrong Format`);
    process.exit(0);
}

//
// Start
if (process.argv.length != 3)
    process.exit(0);

let s;
if (!(s = reg.exec(process.argv[2])))
    die_wrong();

// Get the index of the month and week day
let m = months.indexOf(s[3].toLowerCase());
let wd = days.indexOf(s[1].toLowerCase());

// Check if the indexes are valid
if (m < 0 || wd < 0)
    die_wrong();

let d = new Date(s[4], m, s[2], s[5], s[6], s[7]);
let now = new Date("1970-1-1");

console.log(((d - now) / 1000).toFixed(0));
