const axios = require('axios');
const fs = require('fs');
const path = require('path');

const __basedir = path.dirname(__filename);

const mkdirp = (path) => {
    if (!fs.existsSync(path)) {
        fs.mkdirSync(path, { recursive: true });
    }
};

if (process.argv.length != 3)
    process.exit(0);

let u = null;

try {
    u = new URL(process.argv[2]);
} catch(e) {
    console.error(e);
    process.exit(0);
}

axios.get(u.href)
.then(res => {
    let r = /<img[^>]*(\/)?>/gm;
    let matches = res.data.match(r);
    mkdirp(path.join(__basedir, u.host));

    matches.forEach(m => {
        let src = m.match(/(?<=src=")[^"]*/)[0];

        if (src.charAt(0) == '/')
            src = `${u.origin}${src}`;

        let fname = path.join(__basedir,
                                u.host,
                                src.split('/').splice(-1)[0]);

        axios.get(src, { responseType: 'stream' })
        .then((res) => {
            let writer = fs.createWriteStream(fname);
            res.data.pipe(writer);
        })
        .catch(err => {
            console.error(
                `Could not download file ${fname} from ${src}.\n${err}`
            );
        });
    });
})
.catch(err => {
    console.error(err);
});
