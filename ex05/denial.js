const csvParser = require('csv-parser');
const readline = require('readline');
const fs = require('fs');

let data = [];
var headers = null;

function readLine(rl) {
    // eslint-disable-next-line no-unused-vars
    return new Promise((resolve, _reject) => {
        rl.question('Enter your command: ', (res) => {
            resolve(res);
        });
    });
}

async function handleCommand(key) {
    if (headers.indexOf(key) < 0)
        process.exit(0);

    let rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
        terminal: false
    });

    rl.on('close', () => {
        console.log('^D');
        process.exit(0);
    });

    // eslint-disable-next-line no-constant-condition
    while (true) {
        let a = await readLine(rl);
        try {
            eval(a);
        } catch(e) {
            console.error(e);
        }
    }
}

async function main() {
    if (process.argv.length != 4)
        process.exit(0);

    let f = process.argv[2];
    let k = process.argv[3];

    if (!fs.existsSync(f))
        process.exit(0);

    fs.createReadStream(f)
        .pipe(csvParser({separator: ';'}))
        .on('headers', header => {
            headers = header;
            for (const h of headers) {
                global[h] = {};
            }
        })
        .on('data', row => {
            data.push(row);
            for (const h of headers) {
                global[h][row[k]] = row[h];
            }
        })
        .on('end', async () => { await handleCommand(k); });
}

(async () => {
    await main();
})();
