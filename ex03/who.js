
const exec = require(`child_process`).exec;

const monthNames = [ `Jan`, `Feb`, `Mar`, `Apr`, `May`, `Jun`, `Jul`, `Aug`,
    `Sep`, `Oct`, `Nov`, `Dec` ];

// eslint-disable-next-line no-unused-vars
exec(`w`, (_err, out, _in) => {
    let s = out.trim().split(`\n`);
    for (var i = 2; i < s.length; i++)
    {
        let arr = s[i].split(/\s+/);
        let d = new Date();

        d.setHours(arr[3].split(`:`)[0]);
        d.setMinutes(arr[3].split(`:`)[1]);

        let name = (arr[1] == `console`) ? arr[1] : `tty${arr[1]}`;
        let month = monthNames[d.getMonth()];

        console.log(
            `${arr[0]} ${name.padEnd(8)} ${month} ${d.getDate()} ${arr[3]} `
        );
    }
});
