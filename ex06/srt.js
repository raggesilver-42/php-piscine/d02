const fs = require(`fs`);
const path = require(`path`);

if (process.argv.length != 3)
    process.exit();

const fname = process.argv[2];

let content;

try {
    content = fs.readFileSync(path.join(process.cwd(), fname));
    content = content.toString();

    let arr = content.split(/^$/gm);

    arr = arr.map((val) => val.trim().split(/\n/));

    arr.sort((a, b) => {
        return (a[1].localeCompare(b[1]));
    });

    arr = arr.map((val, i) => {
        val[0] = `${(i + 1)}`;
        val = val.join(`\n`);
        return (val);
    });

    content = arr.join(`\n\n`);
    content += `\n`;

} catch(e) {
    console.error(e);
    process.exit(1);
}

process.stdout.write(content);
