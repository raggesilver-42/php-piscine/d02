<?php

    function replace_cb($m)
    {
        // Replace titles
        $m[0] = preg_replace_callback('/( title=\")(.*?)(\")/mi', function($m) {
            return ($m[1]."".strtoupper($m[2])."".$m[3]);
        }, $m[0]);

        // Replace text in between > <
        $m[0] = preg_replace_callback('/(>)(.*?)(<)/si', function($m) {
            return ($m[1]."".strtoupper($m[2])."".$m[3]);
        }, $m[0]);

        return ($m[0]);
    }

	if ($argc != 2 || !file_exists($argv[1]))
        exit();

	$f = fopen($argv[1], 'r');
	$page = "";

    while ($f && !feof($f))
        $page .= fgets($f);

    $reg = '/(<a )(.*?)(>)(.*)(<\/a>)/si';
    $page = preg_replace_callback($reg, 'replace_cb', $page);

    printf("%s", $page);
